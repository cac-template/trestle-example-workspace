---
x-trestle-global:
  sort-id: cisc-15.02
---

# cisc-15.2 - \[\] Establish and Maintain a Service Provider Management Policy

## Control Statement

Establish and maintain a service provider management policy. Ensure the policy addresses the classification, inventory, assessment, monitoring, and decommissioning of service providers. Review and update the policy annually, or when significant enterprise changes occur that could impact this Safeguard.
