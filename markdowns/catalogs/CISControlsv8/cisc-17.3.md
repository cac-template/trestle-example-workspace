---
x-trestle-global:
  sort-id: cisc-17.03
---

# cisc-17.3 - \[\] Establish and Maintain an Enterprise Process for Reporting Incidents

## Control Statement

Establish and maintain an enterprise process for the workforce to report security incidents. The process includes reporting timeframe, personnel to report to, mechanism for reporting, and the minimum information to be reported. Ensure the process is publicly available to all of the workforce. Review annually, or when significant enterprise changes occur that could impact this Safeguard.
