---
x-trestle-global:
  sort-id: cisc-16.03
---

# cisc-16.3 - \[\] Perform Root Cause Analysis on Security Vulnerabilities

## Control Statement

Perform root cause analysis on security vulnerabilities. When reviewing vulnerabilities, root cause analysis is the task of evaluating underlying issues that create vulnerabilities in code, and allows development teams to move beyond just fixing individual vulnerabilities as they arise.
