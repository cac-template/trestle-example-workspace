---
x-trestle-global:
  sort-id: cisc-07.07
---

# cisc-7.7 - \[\] Remediate Detected Vulnerabilities

## Control Statement

Remediate detected vulnerabilities in software through processes and tooling on a monthly, or more frequent, basis, based on the remediation process.
