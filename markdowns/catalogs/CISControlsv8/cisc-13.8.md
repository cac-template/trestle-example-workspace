---
x-trestle-global:
  sort-id: cisc-13.08
---

# cisc-13.8 - \[\] Deploy a Network Intrusion Prevention Solutions

## Control Statement

Deploy a network intrusion prevention solution, where appropriate. Example implementations include the use of a Network Intrusion Prevention System (NIPS) or equivalent CSP service.
