---
x-trestle-global:
  sort-id: cisc-16.08
---

# cisc-16.8 - \[\] Separate Production and Non-Production Systems

## Control Statement



Maintain separate environments for production and non-production systems.
