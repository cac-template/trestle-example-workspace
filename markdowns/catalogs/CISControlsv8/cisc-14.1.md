---
x-trestle-global:
  sort-id: cisc-14.01
---

# cisc-14.1 - \[\] Establish and Maintain a Security Awareness Program

## Control Statement

Establish and maintain a security awareness program. The purpose of a security awareness program is to educate the enterprise’s workforce on how to interact with enterprise assets and data in a secure manner. Conduct training at hire and, at a minimum, annually. Review and update content annually, or when significant enterprise changes occur that could impact this Safeguard.
