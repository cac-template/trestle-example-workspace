---
x-trestle-global:
  sort-id: cisc-18.02
---

# cisc-18.2 - \[\] Perform Periodic External Penetration Tests

## Control Statement

Perform periodic external penetration tests based on program requirements, no less than annually. External penetration testing must include enterprise and environmental reconnaissance to detect exploitable information. Penetration testing requires specialized skills and experience and must be conducted through a qualified party. The testing may be clear box or opaque box.
