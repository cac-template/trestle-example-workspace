---
x-trestle-global:
  sort-id: cisc-17.09
---

# cisc-17.9 - \[\] Establish and Maintain Security Incident Thresholds

## Control Statement

Establish and maintain security incident thresholds, including, at a minimum, differentiating between an incident and an event. Examples can include: abnormal activity, security vulnerability, security weakness, data breach, privacy incident, etc. Review annually, or when significant enterprise changes occur that could impact this Safeguard.
