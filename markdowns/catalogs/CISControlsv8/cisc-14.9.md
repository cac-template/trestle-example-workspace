---
x-trestle-global:
  sort-id: cisc-14.09
---

# cisc-14.9 - \[\] Conduct Role-Specific Security Awareness and Skills Training

## Control Statement

Conduct role-specific security awareness and skills training. Example implementations include secure system administration courses for IT professionals, (OWASP® Top 10 vulnerability awareness and prevention training for web application developers, and advanced social engineering awareness training for high-profile roles.
