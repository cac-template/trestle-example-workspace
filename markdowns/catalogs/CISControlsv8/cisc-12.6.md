---
x-trestle-global:
  sort-id: cisc-12.06
---

# cisc-12.6 - \[\] Use of Secure Network Management and Communication Protocols

## Control Statement

Use secure network management and communication protocols (e.g., 802.1X, Wi-Fi Protected Access 2 (WPA2) Enterprise or greater).
