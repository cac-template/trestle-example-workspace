---
x-trestle-global:
  sort-id: cisc-18.04
---

# cisc-18.4 - \[\] Validate Security Measures

## Control Statement

Validate security measures after each penetration test. If deemed necessary, modify rulesets and capabilities to detect the techniques used during testing.
