---
x-trestle-global:
  sort-id: cisc-12.08
---

# cisc-12.8 - \[\] Establish and Maintain Dedicated Computing Resources for All Administrative Work

## Control Statement

Establish and maintain dedicated computing resources, either physically or logically separated, for all administrative tasks or tasks requiring administrative access. The computing resources should be segmented from the enterprise’s primary network and not be allowed internet access.
