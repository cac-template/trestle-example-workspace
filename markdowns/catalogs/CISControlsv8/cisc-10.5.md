---
x-trestle-global:
  sort-id: cisc-10.05
---

# cisc-10.5 - \[\] Enable Anti-Exploitation Features

## Control Statement

Enable anti-exploitation features on enterprise assets and software, where possible, such as Microsoft® Data Execution Prevention (DEP), Windows® Defender Exploit Guard (WDEG), or Apple® System Integrity Protection (SIP) and Gatekeeper™.
