---
x-trestle-global:
  sort-id: cisc-09.01
---

# cisc-9.1 - \[\] Ensure Use of Only Fully Supported Browsers and Email Clients

## Control Statement

Ensure only fully supported browsers and email clients are allowed to execute in the enterprise, only using the latest version of browsers and email clients provided through the vendor.
