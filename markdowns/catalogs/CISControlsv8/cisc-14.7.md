---
x-trestle-global:
  sort-id: cisc-14.07
---

# cisc-14.7 - \[\] Train Workforce on How to Identify and Report if Their Enterprise Assets are Missing Security Updates

## Control Statement

Train workforce to understand how to verify and report out-of-date software patches or any failures in automated processes and tools. Part of this training should include notifying IT personnel of any failures in automated processes and tools.
