---
x-trestle-global:
  sort-id: cisc-12.01
---

# cisc-12.1 - \[\] Ensure Network Infrastructure is Up-to-Date

## Control Statement

Ensure network infrastructure is kept up-to-date. Example implementations include running the latest stable release of software and/or using currently supported network-as-a-service (NaaS) offerings. Review software versions monthly, or more frequently, to verify software support.
