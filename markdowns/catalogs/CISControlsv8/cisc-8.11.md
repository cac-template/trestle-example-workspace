---
x-trestle-global:
  sort-id: cisc-08.11
---

# cisc-8.11 - \[\] Conduct Audit Log Reviews

## Control Statement

Conduct reviews of audit logs to detect anomalies or abnormal events that could indicate a potential threat. Conduct reviews on a weekly, or more frequent, basis.
