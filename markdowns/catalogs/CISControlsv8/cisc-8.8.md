---
x-trestle-global:
  sort-id: cisc-08.08
---

# cisc-8.8 - \[\] Collect Command-Line Audit Logs

## Control Statement

Collect command-line audit logs. Example implementations include collecting audit logs from PowerShell®, BASH™, and remote administrative terminals.
