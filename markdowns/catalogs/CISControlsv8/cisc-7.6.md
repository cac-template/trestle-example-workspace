---
x-trestle-global:
  sort-id: cisc-07.06
---

# cisc-7.6 - \[\] Perform Automated Vulnerability Scans of Externally-Exposed Enterprise Assets

## Control Statement

Perform automated vulnerability scans of externally-exposed enterprise assets using a SCAP-compliant vulnerability scanning tool. Perform scans on a monthly, or more frequent, basis.
