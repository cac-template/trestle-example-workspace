---
x-trestle-global:
  sort-id: cisc-11.02
---

# cisc-11.2 - \[\] Perform Automated Backups

## Control Statement

Perform automated backups of in-scope enterprise assets. Run backups weekly, or more frequently, based on the sensitivity of the data.
