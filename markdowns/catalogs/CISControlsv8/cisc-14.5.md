---
x-trestle-global:
  sort-id: cisc-14.05
---

# cisc-14.5 - \[\] Train Workforce Members on Causes of Unintentional Data Exposure

## Control Statement

Train workforce members to be aware of causes for unintentional data exposure. Example topics include mis-delivery of sensitive data, losing a portable end-user device, or publishing data to unintended audiences.
