---
x-trestle-global:
  sort-id: cisc-08.10
---

# cisc-8.10 - \[\] Retain Audit Logs

## Control Statement

Retain audit logs across enterprise assets for a minimum of 90 days.
