---
x-trestle-global:
  sort-id: cisc-13.01
---

# cisc-13.1 - \[\] Centralize Security Event Alerting

## Control Statement

Centralize security event alerting across enterprise assets for log correlation and analysis. Best practice implementation requires the use of a SIEM, which includes vendor-defined event correlation alerts. A log analytics platform configured with security-relevant correlation alerts also satisfies this Safeguard.
