---
x-trestle-global:
  sort-id: cisc-11.04
---

# cisc-11.4 - \[\] Establish and Maintain an Isolated Instance of Recovery Data

## Control Statement

Establish and maintain an isolated instance of recovery data. Example implementations include, version controlling backup destinations through offline, cloud, or off-site systems or services.
