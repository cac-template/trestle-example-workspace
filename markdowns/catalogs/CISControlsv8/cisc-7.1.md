---
x-trestle-global:
  sort-id: cisc-07.01
---

# cisc-7.1 - \[\] Establish and Maintain a Vulnerability Management Process

## Control Statement

Establish and maintain a documented vulnerability management process for enterprise assets. Review and update documentation annually, or when significant enterprise changes occur that could impact this Safeguard.
