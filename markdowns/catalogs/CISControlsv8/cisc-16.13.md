---
x-trestle-global:
  sort-id: cisc-16.13
---

# cisc-16.13 - \[\] Conduct Application Penetration Testing

## Control Statement

Conduct application penetration testing. For critical applications, authenticated penetration testing is better suited to finding business logic vulnerabilities than code scanning and automated security testing. Penetration testing relies on the skill of the tester to manually manipulate an application as an authenticated and unauthenticated user.
