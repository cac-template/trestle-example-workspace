---
x-trestle-global:
  sort-id: cisc-11.05
---

# cisc-11.5 - \[\] Test Data Recovery

## Control Statement

Test backup recovery quarterly, or more frequently, for a sampling of in-scope enterprise assets.
