---
x-trestle-global:
  sort-id: cisc-13.02
---

# cisc-13.2 - \[\] Deploy a Host-Based Intrusion Detection Solution

## Control Statement

Deploy a host-based intrusion detection solution on enterprise assets, where appropriate and/or supported.
