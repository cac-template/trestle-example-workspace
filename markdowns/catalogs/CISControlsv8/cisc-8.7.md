---
x-trestle-global:
  sort-id: cisc-08.07
---

# cisc-8.7 - \[\] Collect URL Request Audit Logs

## Control Statement

Collect URL request audit logs on enterprise assets, where appropriate and supported.
