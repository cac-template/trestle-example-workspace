---
x-trestle-global:
  sort-id: cisc-07.02
---

# cisc-7.2 - \[\] Establish and Maintain a Remediation Process

## Control Statement

Establish and maintain a risk-based remediation strategy documented in a remediation process, with monthly, or more frequent, reviews.
