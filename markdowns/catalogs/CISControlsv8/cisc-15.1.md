---
x-trestle-global:
  sort-id: cisc-15.01
---

# cisc-15.1 - \[\] Establish and Maintain an Inventory of Service Providers

## Control Statement

Establish and maintain an inventory of service providers. The inventory is to list all known service providers, include classification(s), and designate an enterprise contact for each service provider. Review and update the inventory annually, or when significant enterprise changes occur that could impact this Safeguard.
