---
x-trestle-global:
  sort-id: cisc-10.01
---

# cisc-10.1 - \[\] Deploy and Maintain Anti-Malware Software

## Control Statement

Deploy and maintain anti-malware software on all enterprise assets.
