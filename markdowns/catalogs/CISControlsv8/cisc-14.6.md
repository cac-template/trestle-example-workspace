---
x-trestle-global:
  sort-id: cisc-14.06
---

# cisc-14.6 - \[\] Train Workforce Members on Recognizing and Reporting Security Incidents

## Control Statement

Train workforce members to be able to recognize a potential incident and be able to report such an incident.
