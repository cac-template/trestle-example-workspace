---
x-trestle-global:
  sort-id: cisc-15.03
---

# cisc-15.3 - \[\] Classify Service Providers

## Control Statement

Classify service providers. Classification consideration may include one or more characteristics, such as data sensitivity, data volume, availability requirements, applicable regulations, inherent risk, and mitigated risk. Update and review classifications annually, or when significant enterprise changes occur that could impact this Safeguard.
