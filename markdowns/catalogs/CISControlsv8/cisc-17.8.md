---
x-trestle-global:
  sort-id: cisc-17.08
---

# cisc-17.8 - \[\] Conduct Post-Incident Reviews

## Control Statement

Conduct post-incident reviews. Post-incident reviews help prevent incident recurrence through identifying lessons learned and follow-up action.
