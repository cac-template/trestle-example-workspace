---
x-trestle-global:
  sort-id: cisc-10.04
---

# cisc-10.4 - \[\] Configure Automatic Anti-Malware Scanning of Removable Media

## Control Statement

Configure anti-malware software to automatically scan removable media.
