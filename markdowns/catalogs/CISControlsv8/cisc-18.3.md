---
x-trestle-global:
  sort-id: cisc-18.03
---

# cisc-18.3 - \[\] Remediate Penetration Test Findings

## Control Statement

Remediate penetration test findings based on the enterprise’s policy for remediation scope and prioritization.
