---
x-trestle-global:
  sort-id: cisc-07.04
---

# cisc-7.4 - \[\] Perform Automated Application Patch Management

## Control Statement

Perform application updates on enterprise assets through automated patch management on a monthly, or more frequent, basis.
