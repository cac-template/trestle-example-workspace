---
x-trestle-global:
  sort-id: cisc-08.12
---

# cisc-8.12 - \[\] Collect Service Provider Logs

## Control Statement

Collect service provider logs, where supported. Example implementations include collecting authentication and authorization events, data creation and disposal events, and user management events.
