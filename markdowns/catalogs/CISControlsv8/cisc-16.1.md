---
x-trestle-global:
  sort-id: cisc-16.01
---

# cisc-16.1 - \[\] Establish and Maintain a Secure Application Development Process

## Control Statement

Establish and maintain a secure application development process. In the process, address such items as: secure application design standards, secure coding practices, developer training, vulnerability management, security of third-party code, and application security testing procedures. Review and update documentation annually, or when significant enterprise changes occur that could impact this Safeguard.
