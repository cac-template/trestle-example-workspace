---
x-trestle-global:
  sort-id: cisc-09.07
---

# cisc-9.7 - \[\] Deploy and Maintain Email Server Anti-Malware Protections

## Control Statement

Deploy and maintain email server anti-malware protections, such as attachment scanning and/or sandboxing.
