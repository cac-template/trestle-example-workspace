---
x-trestle-global:
  sort-id: cisc-09.06
---

# cisc-9.6 - \[\] Block Unnecessary File Types

## Control Statement

Block unnecessary file types attempting to enter the enterprise's email gateway.
