---
x-trestle-global:
  sort-id: cisc-10.06
---

# cisc-10.6 - \[\] Centrally Manage Anti-Malware Software

## Control Statement

Centrally manage anti-malware software.
