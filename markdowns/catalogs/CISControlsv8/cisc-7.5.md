---
x-trestle-global:
  sort-id: cisc-07.05
---

# cisc-7.5 - \[\] Perform Automated Vulnerability Scans of Internal Enterprise Assets

## Control Statement

Perform automated vulnerability scans of internal enterprise assets on a quarterly, or more frequent, basis. Conduct both authenticated and unauthenticated scans, using a SCAP-compliant vulnerability scanning tool.
