---
x-trestle-global:
  sort-id: cisc-14.02
---

# cisc-14.2 - \[\] Train Workforce Members to Recognize Social Engineering Attacks

## Control Statement

Train workforce members to recognize social engineering attacks, such as phishing, pre-texting, and tailgating.
