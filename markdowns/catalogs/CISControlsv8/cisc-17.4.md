---
x-trestle-global:
  sort-id: cisc-17.04
---

# cisc-17.4 - \[\] Establish and Maintain an Incident Response Process

## Control Statement

Establish and maintain an incident response process that addresses roles and responsibilities, compliance requirements, and a communication plan. Review annually, or when significant enterprise changes occur that could impact this Safeguard.
