---
x-trestle-global:
  sort-id: cisc-14.03
---

# cisc-14.3 - \[\] Train Workforce Members on Authentication Best Practices

## Control Statement

Train workforce members on authentication best practices. Example topics include MFA, password composition, and credential management.
