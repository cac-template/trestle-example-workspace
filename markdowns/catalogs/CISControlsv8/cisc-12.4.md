---
x-trestle-global:
  sort-id: cisc-12.04
---

# cisc-12.4 - \[\] Establish and Maintain Architecture Diagram(s)

## Control Statement

Establish and maintain architecture diagram(s) and/or other network system documentation. Review and update documentation annually, or when significant enterprise changes occur that could impact this Safeguard.
