---
x-trestle-global:
  sort-id: cisc-13.03
---

# cisc-13.3 - \[\] Deploy a Network Intrusion Detection Solution

## Control Statement

Deploy a network intrusion detection solution on enterprise assets, where appropriate. Example implementations include the use of a Network Intrusion Detection System (NIDS) or equivalent cloud service provider (CSP) service.
