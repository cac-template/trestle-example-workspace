---
x-trestle-global:
  sort-id: cisc-17.07
---

# cisc-17.7 - \[\] Conduct Routine Incident Response Exercises

## Control Statement

Plan and conduct routine incident response exercises and scenarios for key personnel involved in the incident response process to prepare for responding to real-world incidents. Exercises need to test communication channels, decision making, and workflows. Conduct testing on an annual basis, at a minimum.
