---
x-trestle-global:
  sort-id: cisc-13.05
---

# cisc-13.5 - \[\] Manage Access Control for Remote Assets

## Control Statement

Manage access control for assets remotely connecting to enterprise resources. Determine amount of access to enterprise resources based on: up-to-date anti-malware software installed, configuration compliance with the enterprise’s secure configuration process, and ensuring the operating system and applications are up-to-date.
