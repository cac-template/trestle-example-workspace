---
x-trestle-global:
  sort-id: cisc-09.05
---

# cisc-9.5 - \[\] Implement DMARC

## Control Statement

To lower the chance of spoofed or modified emails from valid domains, implement DMARC policy and verification, starting with implementing the Sender Policy Framework (SPF) and the DomainKeys Identified Mail (DKIM) standards.
