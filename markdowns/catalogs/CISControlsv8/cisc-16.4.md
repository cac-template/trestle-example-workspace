---
x-trestle-global:
  sort-id: cisc-16.04
---

# cisc-16.4 - \[\] Establish and Manage an Inventory of Third-Party Software Components

## Control Statement

Establish and manage an updated inventory of third-party components used in development, often referred to as a “bill of materials,” as well as components slated for future use. This inventory is to include any risks that each third-party component could pose. Evaluate the list at least monthly to identify any changes or updates to these components, and validate that the component is still supported.
