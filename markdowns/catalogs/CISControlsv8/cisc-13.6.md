---
x-trestle-global:
  sort-id: cisc-13.06
---

# cisc-13.6 - \[\] Collect Network Traffic Flow Logs

## Control Statement

Collect network traffic flow logs and/or network traffic to review and alert upon from network devices.
