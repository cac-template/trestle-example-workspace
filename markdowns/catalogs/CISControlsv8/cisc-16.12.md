---
x-trestle-global:
  sort-id: cisc-16.12
---

# cisc-16.12 - \[\] Implement Code-Level Security Checks

## Control Statement

Apply static and dynamic analysis tools within the application life cycle to verify that secure coding practices are being followed.
