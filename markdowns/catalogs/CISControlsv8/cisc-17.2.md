---
x-trestle-global:
  sort-id: cisc-17.02
---

# cisc-17.2 - \[\] Establish and Maintain Contact Information for Reporting Security Incidents

## Control Statement

Establish and maintain contact information for parties that need to be informed of security incidents. Contacts may include internal staff, third-party vendors, law enforcement, cyber insurance providers, relevant government agencies, Information Sharing and Analysis Center (ISAC) partners, or other stakeholders. Verify contacts annually to ensure that information is up-to-date.
