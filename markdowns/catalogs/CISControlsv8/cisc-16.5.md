---
x-trestle-global:
  sort-id: cisc-16.05
---

# cisc-16.5 - \[\] Use Up-to-Date and Trusted Third-Party Software Components

## Control Statement

Use up-to-date and trusted third-party software components. When possible, choose established and proven frameworks and libraries that provide adequate security. Acquire these components from trusted sources or evaluate the software for vulnerabilities before use.
