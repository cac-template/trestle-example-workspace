---
x-trestle-global:
  sort-id: cisc-10.07
---

# cisc-10.7 - \[\] Use Behavior-Based Anti-Malware Software

## Control Statement

Use behavior-based anti-malware software.
