---
x-trestle-global:
  sort-id: cisc-08.09
---

# cisc-8.9 - \[\] Centralize Audit Logs

## Control Statement

Centralize, to the extent possible, audit log collection and retention across enterprise assets.
