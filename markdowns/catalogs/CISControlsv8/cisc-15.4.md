---
x-trestle-global:
  sort-id: cisc-15.04
---

# cisc-15.4 - \[\] Ensure Service Provider Contracts Include Security Requirements

## Control Statement

Ensure service provider contracts include security requirements. Example requirements may include minimum security program requirements, security incident and/or data breach notification and response, data encryption requirements, and data disposal commitments. These security requirements must be consistent with the enterprise’s service provider management policy. Review service provider contracts annually to ensure contracts are not missing security requirements.
