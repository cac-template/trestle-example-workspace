---
x-trestle-global:
  sort-id: cisc-17.05
---

# cisc-17.5 - \[\] Assign Key Roles and Responsibilities

## Control Statement

Assign key roles and responsibilities for incident response, including staff from legal, IT, information security, facilities, public relations, human resources, incident responders, and analysts, as applicable. Review annually, or when significant enterprise changes occur that could impact this Safeguard.
