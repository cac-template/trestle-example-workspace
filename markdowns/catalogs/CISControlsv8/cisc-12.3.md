---
x-trestle-global:
  sort-id: cisc-12.03
---

# cisc-12.3 - \[\] Securely Manage Network Infrastructure

## Control Statement

Securely manage network infrastructure. Example implementations include version-controlled-infrastructure-as-code, and the use of secure network protocols, such as SSH and HTTPS.
