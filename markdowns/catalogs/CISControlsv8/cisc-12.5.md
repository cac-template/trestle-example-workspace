---
x-trestle-global:
  sort-id: cisc-12.05
---

# cisc-12.5 - \[\] Centralize Network Authentication, Authorization, and Auditing (AAA)

## Control Statement

Centralize network AAA
