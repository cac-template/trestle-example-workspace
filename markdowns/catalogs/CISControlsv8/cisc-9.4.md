---
x-trestle-global:
  sort-id: cisc-09.04
---

# cisc-9.4 - \[\] Restrict Unnecessary or Unauthorized Browser and Email Client Extensions

## Control Statement

Restrict, either through uninstalling or disabling, any unauthorized or unnecessary browser or email client plugins, extensions, and add-on applications.
