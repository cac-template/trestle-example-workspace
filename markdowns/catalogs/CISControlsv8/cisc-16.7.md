---
x-trestle-global:
  sort-id: cisc-16.07
---

# cisc-16.7 - \[\] Use Standard Hardening Configuration Templates for Application Infrastructure

## Control Statement

Use standard, industry-recommended hardening configuration templates for application infrastructure components. This includes underlying servers, databases, and web servers, and applies to cloud containers, Platform as a Service (PaaS) components, and SaaS components. Do not allow in-house developed software to weaken configuration hardening.
