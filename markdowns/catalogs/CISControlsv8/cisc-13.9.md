---
x-trestle-global:
  sort-id: cisc-13.09
---

# cisc-13.9 - \[\] Deploy Port-Level Access Control

## Control Statement

Deploy port-level access control. Port-level access control utilizes 802.1x, or similar network access control protocols, such as certificates, and may incorporate user and/or device authentication.
