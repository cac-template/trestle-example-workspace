---
x-trestle-global:
  sort-id: cisc-15.06
---

# cisc-15.6 - \[\] Monitor Service Providers

## Control Statement

Monitor service providers consistent with the enterprise’s service provider management policy. Monitoring may include periodic reassessment of service provider compliance, monitoring service provider release notes, and dark web monitoring.
