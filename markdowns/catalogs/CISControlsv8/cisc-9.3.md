---
x-trestle-global:
  sort-id: cisc-09.03
---

# cisc-9.3 - \[\] Maintain and Enforce Network-Based URL Filters

## Control Statement

Enforce and update network-based URL filters to limit an enterprise asset from connecting to potentially malicious or unapproved websites. Example implementations include category-based filtering, reputation-based filtering, or through the use of block lists. Enforce filters for all enterprise assets.
