---
x-trestle-global:
  sort-id: cisc-07.03
---

# cisc-7.3 - \[\] Perform Automated Operating System Patch Management

## Control Statement

Perform operating system updates on enterprise assets through automated patch management on a monthly, or more frequent, basis.
