---
x-trestle-global:
  sort-id: cisc-15.07
---

# cisc-15.7 - \[\] Securely Decommission Service Providers

## Control Statement

Securely decommission service providers. Example considerations include user and service account deactivation, termination of data flows, and secure disposal of enterprise data within service provider systems
