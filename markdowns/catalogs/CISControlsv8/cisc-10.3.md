---
x-trestle-global:
  sort-id: cisc-10.03
---

# cisc-10.3 - \[\] Disable Autorun and Autoplay for Removable Media

## Control Statement

Disable autorun and autoplay auto-execute functionality for removable media.
