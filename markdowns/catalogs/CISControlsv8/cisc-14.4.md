---
x-trestle-global:
  sort-id: cisc-14.04
---

# cisc-14.4 - \[\] Train Workforce on Data Handling Best Practices

## Control Statement

Train workforce members on how to identify and properly store, transfer, archive, and destroy sensitive data. This also includes training workforce members on clear screen and desk best practices, such as locking their screen when they step away from their enterprise asset, erasing physical and virtual whiteboards at the end of meetings, and storing data and assets securely.
