---
x-trestle-global:
  sort-id: cisc-13.04
---

# cisc-13.4 - \[\] Perform Traffic Filtering Between Network Segments

## Control Statement

Perform traffic filtering between network segments, where appropriate.
