---
x-trestle-global:
  sort-id: cisc-11.03
---

# cisc-11.3 - \[\] Protect Recovery Data

## Control Statement

Protect recovery data with equivalent controls to the original data. Reference encryption or data separation, based on requirements.
