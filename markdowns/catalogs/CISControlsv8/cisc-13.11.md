---
x-trestle-global:
  sort-id: cisc-13.11
---

# cisc-13.11 - \[\] Tune Security Event Alerting Thresholds

## Control Statement

Tune security event alerting thresholds monthly, or more frequently.
