---
x-trestle-global:
  sort-id: cisc-12.07
---

# cisc-12.7 - \[\] Ensure Remote Devices Utilize a VPN and are Connecting to an Enterprise’s AAA Infrastructure

## Control Statement

Require users to authenticate to enterprise-managed VPN and authentication services prior to accessing enterprise resources on end-user devices.
