---
x-trestle-global:
  sort-id: cisc-13.10
---

# cisc-13.10 - \[\] Perform Application Layer Filtering

## Control Statement

Perform application layer filtering. Example implementations include a filtering proxy, application layer firewall, or gateway.
