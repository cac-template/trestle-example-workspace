---
x-trestle-global:
  sort-id: cisc-18.05
---

# cisc-18.5 - \[\] Perform Periodic Internal Penetration Tests

## Control Statement

Perform periodic internal penetration tests based on program requirements, no less than annually. The testing may be clear box or opaque box.
