---
x-trestle-global:
  sort-id: cisc-09.02
---

# cisc-9.2 - \[\] Use DNS Filtering Services

## Control Statement

Use DNS filtering services on all enterprise assets to block access to known malicious domains.
