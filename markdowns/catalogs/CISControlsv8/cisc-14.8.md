---
x-trestle-global:
  sort-id: cisc-14.08
---

# cisc-14.8 - \[\] Train Workforce on the Dangers of Connecting to and Transmitting Enterprise Data Over Insecure Networks

## Control Statement

Train workforce members on the dangers of connecting to, and transmitting data over, insecure networks for enterprise activities. If the enterprise has remote workers, training must include guidance to ensure that all users securely configure their home network infrastructure.
