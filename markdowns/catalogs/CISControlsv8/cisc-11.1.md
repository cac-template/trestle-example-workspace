---
x-trestle-global:
  sort-id: cisc-11.01
---

# cisc-11.1 - \[\] Establish and Maintain a Data Recovery Process

## Control Statement

Establish and maintain a data recovery process. In the process, address the scope of data recovery activities, recovery prioritization, and the security of backup data. Review and update documentation annually, or when significant enterprise changes occur that could impact this Safeguard.
