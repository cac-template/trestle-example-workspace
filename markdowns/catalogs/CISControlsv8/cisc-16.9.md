---
x-trestle-global:
  sort-id: cisc-16.09
---

# cisc-16.9 - \[\] Train Developers in Application Security Concepts and Secure Coding

## Control Statement

Ensure that all software development personnel receive training in writing secure code for their specific development environment and responsibilities. Training can include general security principles and application security standard practices. Conduct training at least annually and design in a way to promote security within the development team, and build a culture of security among the developers.
