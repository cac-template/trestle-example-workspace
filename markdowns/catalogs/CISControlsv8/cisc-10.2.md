---
x-trestle-global:
  sort-id: cisc-10.02
---

# cisc-10.2 - \[\] Configure Automatic Anti-Malware Signature Updates

## Control Statement

Configure automatic updates for anti-malware signature files on all enterprise assets.
