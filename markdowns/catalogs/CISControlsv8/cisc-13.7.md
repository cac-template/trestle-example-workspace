---
x-trestle-global:
  sort-id: cisc-13.07
---

# cisc-13.7 - \[\] Deploy a Host-Based Intrusion Prevention Solution

## Control Statement

Deploy a host-based intrusion prevention solution on enterprise assets, where appropriate and/or supported. Example implementations include use of an Endpoint Detection and Response (EDR) client or host-based IPS agent.
