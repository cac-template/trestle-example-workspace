---
x-trestle-global:
  sort-id: cisc-12.02
---

# cisc-12.2 - \[\] Establish and Maintain a Secure Network Architecture

## Control Statement

Establish and maintain a secure network architecture. A secure network architecture must address segmentation, least privilege, and availability, at a minimum.
