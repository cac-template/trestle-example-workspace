ORG_NAME=NEWNGO

pre-commit: 
	pre-commit install

install:
	python3 -m pip install --upgrade pip
	python3 -m pip install compliance-trestle
	python3 -m pip install pandas gitpython
	python3 -m pip install pre-commit
	#python3 -m pip install python-semantic-release
	pre-commit install

mdformat:
	pre-commit run mdformat --all-files

create-comp-def:
	trestle task csv-to-oscal-cd -c assets/csv-to-oscal-cd.config

update-oscal:
	trestle author catalog-assemble -m md_${ORG_NAME}_custom_catalog -o ${ORG_NAME}_custom
	trestle author profile-assemble -m md_${ORG_NAME}_official_profile -o ${ORG_NAME}_Official --set-parameters
	trestle author profile-assemble -m md_${ORG_NAME}_internal_guidance_profile -o ${ORG_NAME}_int_guidance
	trestle author component-assemble -m md_${ORG_NAME}_comp_def -o ${ORG_NAME}_comp_def
	trestle author ssp-assemble -m md_${ORG_NAME}_platform_ssp -cd ${ORG_NAME}_comp_def -o ${ORG_NAME}_SSP

update-markdown: update-oscal
	trestle author catalog-generate --name ${ORG_NAME}_custom --output md_${ORG_NAME}_custom_catalog
	trestle author profile-generate --name ${ORG_NAME}_Official --output md_${ORG_NAME}_official_profile -y assets/extra-profile-metadata.yml
	trestle author profile-generate --name ${ORG_NAME}_int_guidance --output md_${ORG_NAME}_internal_guidance_profile -y assets/extra-ssp-metadata.yml
	trestle author component-generate --name ${ORG_NAME}_comp_def --output md_${ORG_NAME}_comp_def
	trestle author ssp-generate -cd ${ORG_NAME}_comp_def --profile ${ORG_NAME}_int_guidance --output md_${ORG_NAME}_platform_ssp -y assets/extra-ssp-metadata.yml

initialize-markdown: create-comp-def
	trestle author catalog-generate --name ${ORG_NAME}_custom --output md_${ORG_NAME}_custom_catalog
	trestle author profile-generate --name ${ORG_NAME}_Official --output md_${ORG_NAME}_official_profile -y assets/extra-profile-metadata.yml
	trestle author profile-generate --name ${ORG_NAME}_int_guidance --output md_${ORG_NAME}_internal_guidance_profile -y assets/extra-ssp-metadata.yml
	trestle author component-generate --name ${ORG_NAME}_comp_def --output md_${ORG_NAME}_comp_def
	trestle author ssp-generate -cd ${ORG_NAME}_comp_def --profile ${ORG_NAME}_int_guidance --output md_${ORG_NAME}_platform_ssp -y assets/extra-ssp-metadata.yml
	python -m insert_response_prose

ssp-markdown: update-markdown
	trestle author jinja -i ${ORG_NAME}_platform_ssp.md.jinja -ssp ${ORG_NAME}_SSP -p ${ORG_NAME}_int_guidance -o ${ORG_NAME}_platform_ssp.md -bf "[.]" -vap "${ORG_NAME} Assigned:" -vnap "Assignment:"

ssp-word: ssp-markdown
	pandoc ${ORG_NAME}_platform_ssp.md --from markdown+table_captions+implicit_figures+rebase_relative_paths -t docx --reference-doc ssp_word_template.docx -s --toc -o ${ORG_NAME}_platform_ssp.docx

clean:
	rm -rf md_*
	rm -rf component-definitions/${ORG_NAME}
	rm -rf system-security-plans/${ORG_NAME}_SSP
	rm -f ${ORG_NAME}_platform_ssp.md ${ORG_NAME}_platform_ssp.docx
