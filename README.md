# Install trestle

Create a virtual environment with python3
``` bash
python3 -m venv venv
```

Source the virtual environment
``` bash
source venv/bin/activate
```

Install trestle
```
pip3 install compliance-trestle
```

# Getting started

## Steps
### Import catalog or catalogs
NIST SP800-53 is faily large, to reduce the entry barrier we recommend using low_baseline from NIST or CISv8

To use CIS, you'll need to convert from spreadsheet to the OSCAL catalog, follow the steps in https://github.com/IBM/compliance-trestle-demos/tree/develop/CIS_controls

2. Import profile or profiles
3. Create markdown files
   1. Use `author catalog-generate` command to generate markdown version
4. Modify the markdown according to your needs
5. Track the changes in git.

### Import a catalog
```
trestle import -f https://raw.githubusercontent.com/usnistgov/oscal-content/master/nist.gov/SP800-53/rev5/json/NIST_SP-800-53_rev5_catalog.json -o NIST_SP-800-53_rev5

```

### Import profiles
```
trestle import -f https://raw.githubusercontent.com/usnistgov/oscal-content/master/nist.gov/SP800-53/rev5/json/NIST_SP-800-53_rev5_LOW-baseline_profile.json -o NIST_SP-800-53_rev5_LOW-baseline
```

### Replicate a profile
```
trestle replicate profile -n NIST_SP-800-53_rev5_LOW-baseline -o Example-profile
```
```
trestle href -n Example-profile -hr trestle://catalogs/SP-800-53_rev5
```

### Generate y Assemble
Generate markdown from json catalog
```
trestle author catalog-generate -n CISControlsv8 -o markdowns/CISControlsv8
```


Assemble catalog to json
```
trestle author catalog-assemble -m markdowns/CISControlsv8 -o CISControlsv8
``` 
# References
* https://controlfreak.risk-redux.io/controls

## IBM articles
* https://dzone.com/articles/compass-compliance-part-1
* https://dzone.com/articles/compliance-automated-standard-solution-compass-part-2-trestle-sdk
* https://dzone.com/articles/compliance-automated-standard-solution-compass-part-3-artifacts-and-personas

## NIST OSCAL references and resources
* https://pages.nist.gov/OSCAL/resources/
* https://pages.nist.gov/OSCAL/resources/concepts/

## CIS
* Mapping and compliance: https://www.cisecurity.org/cybersecurity-tools/mapping-compliance
* Mapping tool: https://www.cisecurity.org/controls/cis-controls-navigator
* Script to transform spreadsheet to OSCAL: https://github.com/IBM/compliance-trestle-demos/tree/develop/CIS_controls

## RedHat
* https://github.com/RedHatProductSecurity/trestle-bot
* https://github.com/RedHatProductSecurity/oscal-component-definitions

* https://github.com/oscal-compass/compliance-trestle
* https://github.com/usnistgov/oscal-content
* https://github.com/CISecurity/CISControls_OSCAL
* https://www.cisecurity.org/insights/blog/introducing-the-cis-controls-oscal-repository
* https://github.com/IBM/compliance-trestle-demos/
* https://github.com/oscal-club/awesome-oscal
